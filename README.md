Building the benchmark
----------------------

Set up oneAPI environment:

```
"C:\Program Files (x86)\Intel\oneAPI\setvars.bat"
set CXX=icx-cl.exe
```

Clone and build libspng (optional):

```
mkdir c:\dev\oneapi\i\
set PATH=c:\dev\oneapi\i\bin;%PATH%
git clone https://github.com/dimula73/libspng.git -b kazakov/fix-cmake-include
cmake . -DCMAKE_INSTALL_PREFIX=c:/dev/oneapi/i
cmake --build . --target install
```

Build the benchmark itself:

```
cmake -G Ninja ..\BrushTest -DCMAKE_PREFIX_PATH=c:/dev/oneapi/i
ninja
```


To select the backend on the runtime use one of the following variables:

```
set ONEAPI_DEVICE_SELECTOR="*:cpu"
set ONEAPI_DEVICE_SELECTOR="*:gpu"
```

Environment variables for QtCreator
---------------------

```
CXX=icx-cl.exe
# Copy the following vars from the terminal
LIB=...
Path=...
INCLUDE=...
```

