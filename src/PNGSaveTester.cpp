#include "PNGSaveTester.h"

#include <iostream>
#include <vector>
#include "PNGEncoder.h"



int main()
{
    const size_t bufSize = 5000*5000*4;

    std::vector<char> pixel({255,0,0,255});
    std::vector<char> buf(bufSize);

    for (auto dstIt = buf.begin(); dstIt != buf.end();) {
        dstIt = std::copy(pixel.begin(), pixel.end(), dstIt);
    }

    PNGEncoder enc("test_file.png");
    enc.encode(buf.data(), 5000, 5000);

    std::cout << "exiting normally" << std::endl;
}
