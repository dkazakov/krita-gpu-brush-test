#ifndef TRIVIALCIRCLEMASKGENERATOR_H
#define TRIVIALCIRCLEMASKGENERATOR_H

#include <QtGlobal>
#include "kis_global.h"

struct TrivialCircleMaskGenerator
{

    float m_diameter, m_ratio;
    float m_softness;
    float m_fh, m_fv;
    float m_cs, m_ss;
    float m_cachedSpikesAngle;
    int m_spikes;
    bool m_empty;
    bool m_antialiasEdges;
    float m_scaleX;
    float m_scaleY;

    float m_xcoef, m_ycoef;
    float m_xfadecoef, m_yfadecoef;
    float m_safeSoftnessCoeff;
    float m_transformedFadeX, m_transformedFadeY;

    TrivialCircleMaskGenerator(float _diameter, float _ratio, float _fh, float _fv, int _spikes, bool _antialiasEdges)
    {
        m_diameter = _diameter;
        m_ratio = _ratio;
        m_fh = 0.5f * _fh;
        m_fv = 0.5f * _fv;
        m_softness = 1.0f; // by default don't change fade/softness/hardness
        m_spikes = _spikes;
        m_cachedSpikesAngle = KIS_M_PI / m_spikes;
        m_antialiasEdges = _antialiasEdges;
        m_scaleX = 1.0f;
        m_scaleY = 1.0f;
        init();
        setScale(1.0f, 1.0f);
    }

    void init()
    {
        m_cs = cos(- 2.0f * KIS_M_PI / m_spikes);
        m_ss = sin(- 2.0f * KIS_M_PI / m_spikes);
    }

    template <typename T>
    static inline T norme(T x, T y) {
        return x * x + y * y;
    }

    inline void fixRotation(float &xr, float &yr) const
    {
        if (m_spikes > 2) {
            float angle = (atan2(yr, xr));

            while (angle > m_cachedSpikesAngle){
                float sx = xr;
                float sy = yr;

                xr = m_cs * sx - m_ss * sy;
                yr = m_ss * sx + m_cs * sy;

                angle -= 2 * m_cachedSpikesAngle;
            }
        }
    }

    inline quint8 valueAt(float x, float y) const
    {
        float xr = (x /*- m_xcenter*/);
        float yr = qAbs(y /*- m_ycenter*/);
        fixRotation(xr, yr);

        float n = norme(xr * m_xcoef, yr * m_ycoef);
        if (n > 1.0) return 255;

        // we add +1.0 to ensure correct antialiasing on the border
        if (m_antialiasEdges) {
            xr = abs(xr) + 1.0f;
            yr = abs(yr) + 1.0f;
        }

        float nf = norme(xr * m_transformedFadeX,
                         yr * m_transformedFadeY);

        if (nf < 1.0f) return 0;
        return 255 * n * (nf - 1.0f) / (nf - n);
    }

    void setScale(float scaleX, float scaleY)
    {
        m_scaleX = scaleX;
        m_scaleY = scaleY;

        m_xcoef = 2.0f / effectiveSrcWidth();
        m_ycoef = 2.0f / effectiveSrcHeight();
        m_xfadecoef = qFuzzyCompare(horizontalFade(), 0) ? 1 : (2.0f / (horizontalFade() * effectiveSrcWidth()));
        m_yfadecoef = qFuzzyCompare(verticalFade()  , 0) ? 1 : (2.0f / (verticalFade() * effectiveSrcHeight()));
        m_transformedFadeX = m_xfadecoef * m_safeSoftnessCoeff;
        m_transformedFadeY = m_yfadecoef * m_safeSoftnessCoeff;
    }

    float width() const
    {
        return m_diameter;
    }

    float height() const
    {
        if (m_spikes == 2) {
            return m_diameter * m_ratio;
        }
        return m_diameter;
    }

    float effectiveSrcWidth() const
    {
        return m_diameter * m_scaleX;
    }

    float effectiveSrcHeight() const
    {
        /**
     * This height is related to the source of the brush mask, so we
     * don't take spikes into account, they will be generated from
     * this data.
     */
        return m_diameter * m_ratio * m_scaleY;
    }

    float diameter() const
    {
        return m_diameter;
    }

    float horizontalFade() const
    {
        return 2.0f * m_fh; // 'cause in init we divide it again
    }

    float verticalFade() const
    {
        return 2.0f * m_fv; // 'cause in init we divide it again
    }
};

#endif // TRIVIALCIRCLEMASKGENERATOR_H
