#include <sycl/sycl.hpp>
#include <array>
#include <iostream>
#include <string>

#include <QSize>
#include <QRect>

#include "TrivialCircleMaskGenerator.h"
#include "PixelProcessors.h"

#include <config-spng.h>

#ifdef HAVE_SPNG
#include "PNGEncoder.h"
#endif // HAVE_SPNG

using namespace sycl;

// Create an exception handler for asynchronous SYCL exceptions
static auto exception_handler = [](sycl::exception_list e_list) {
  for (std::exception_ptr const &e : e_list) {
    try {
      std::rethrow_exception(e);
    }
    catch (std::exception const &e) {
      std::cout << "Failure" << std::endl;
      std::terminate();
    }
  }
};

int main(int argc, char* argv[]) {
  auto selector = default_selector_v;

  const int pixelSize = 4;

  QRect imageRect(0, 0, 5000, 5000);
  QRect dabRect(0, 0, 500, 500);
  const size_t dabSize = pixelSize * dabRect.width() * dabRect.height();
  const size_t bufferSize = pixelSize * imageRect.width() * imageRect.height();
  std::vector<quint8> color({255, 0, 0, 255});

  TrivialCircleMaskGenerator gen(dabRect.width(), 0.2, 0.9, 0.7, 2, true);

  try {
    queue q(selector, exception_handler);

    // Print out the device information used for the kernel code.
    std::cout << "Running on device: "
              << q.get_device().get_info<info::device::name>() << "\n";

    std::cout << "Start allocation" << std::endl;

    quint8 *fullImageDevice = malloc_device<quint8>(bufferSize, q);
    quint8 *dabImageDevice = malloc_device<quint8>(dabSize, q);
    quint8 *fullImageHost = malloc_host<quint8>(bufferSize, q);

    if (!fullImageDevice || !fullImageHost || !dabImageDevice) {
        if (fullImageDevice) free(fullImageDevice, q);
        if (fullImageHost) free(fullImageDevice, q);
        if (dabImageDevice) free(dabImageDevice, q);

        std::cout << "Memory allocation failure.\n" << std::endl;
        return -1;
    }
    std::cout << "Start cloning color" << std::endl;

    quint8 *deviceColor = malloc_device<quint8>(pixelSize, q);
    assert(deviceColor);
    q.memcpy(deviceColor, color.data(), pixelSize);
    q.wait();

    std::cout << "End cloning color" << std::endl;

    // precompile all the kernels of the context
    auto myBundle = get_kernel_bundle<bundle_state::executable>(q.get_context());

    enum RenderingMode {
        Direct,
        TwoStageOnePass,
        TwoStageTwoPass
    };

    std::vector<std::pair<RenderingMode, std::pair<int,int>>> testingModes({
        {Direct, {1,1}},
        {TwoStageOnePass, {1,1}},
        {TwoStageTwoPass, {0,5}},
        {TwoStageTwoPass, {1,5}},
        {TwoStageTwoPass, {2,5}},
        {TwoStageTwoPass, {3,5}},
        {TwoStageTwoPass, {4,5}},
        });

    for (auto it = testingModes.begin(); it != testingModes.end(); ++it) {

          RenderingMode mode = it->first;
          const std::pair<int,int> &cachedDabsFraction = it->second;

          const std::string modeString =
              mode == Direct ? "direct" :
                  mode == TwoStageOnePass ? "separate dab, one pass" :
                  mode == TwoStageTwoPass ? "separate dab, two passes" : "<unknown>";

          std::cout << "==========================" << std::endl;
          std::cout << "Start compute in mode: " << modeString << std::endl;
          auto start = std::chrono::high_resolution_clock::now();


          std::optional<event> dabEvent;

          int numDabsPainted = 0;
          int numDabsGenerated = 0;

          for (int y = 0; y < 4500; y += 10) {
              for (int x = 0; x < 4500; x += 10) {
                  float angle = (x + y) * (KIS_M_PI / 6);

                  const float opacity = 0.0f + float((x)) / 4500;

                  MaskProcessingData data(deviceColor, pixelSize,
                                          0.0, 1.0,
                                          0.5 * dabRect.width(), 0.5 * dabRect.width(), angle);
                  std::bitset<4> channelFlags(ULLONG_MAX);

                  if (mode == Direct) {

                      auto newEvent = q.submit([&](handler& cgh) {
                          // Calling use_kernel_bundle() causes the parallel_for() below to use the
                          // pre-compiled kernel from "myBundle".
                          cgh.use_kernel_bundle(myBundle);

                          if (dabEvent) {
                              cgh.depends_on(*dabEvent);
                          }

                          range<2> imageRange(dabRect.height(), dabRect.width());
                          id<2> dabOffset(y, x);

                          cgh.parallel_for(imageRange, [=](id<2> idx) {
                              id<2> imagePos = idx + dabOffset;

                              quint8 *vectorPixel = fullImageDevice + pixelSize * (imagePos[0] * imageRect.width() + imagePos[1]);
                              processPixel<true>(vectorPixel, idx[1], idx[0], &gen, &data, channelFlags, opacity);
                          });
                      });

                      dabEvent = newEvent;

                  } else if (mode == TwoStageOnePass) {

                      auto newEvent = q.submit([&](handler& cgh) {
                          // Calling use_kernel_bundle() causes the parallel_for() below to use the
                          // pre-compiled kernel from "myBundle".
                          cgh.use_kernel_bundle(myBundle);

                          if (dabEvent) {
                              cgh.depends_on(*dabEvent);
                          }

                          range<2> imageRange(dabRect.height(), dabRect.width());
                          id<2> dabOffset(y, x);

                          cgh.parallel_for(imageRange, [=](id<2> idx) {
                              id<2> imagePos = idx + dabOffset;

                              quint8 *dabPixel = dabImageDevice + pixelSize * (idx[0] * dabRect.width() + idx[1]);
                              quint8 *vectorPixel = fullImageDevice + pixelSize * (imagePos[0] * imageRect.width() + imagePos[1]);
                              processPixel<false>(dabPixel, idx[1], idx[0], &gen, &data, channelFlags, opacity);
                              composePixel(dabPixel, vectorPixel, &data, opacity, channelFlags);
                          });
                      });

                      dabEvent = newEvent;

                  } else if (mode == TwoStageTwoPass) {

                      const int chunkPosition = numDabsPainted % cachedDabsFraction.second;
                      if (chunkPosition < (cachedDabsFraction.second - cachedDabsFraction.first)) {

                          auto prefillEvent = q.submit([&](handler& cgh) {
                              // Calling use_kernel_bundle() causes the parallel_for() below to use the
                              // pre-compiled kernel from "myBundle".
                              cgh.use_kernel_bundle(myBundle);

                              if (dabEvent) {
                                  cgh.depends_on(*dabEvent);
                              }

                              range<2> imageRange(dabRect.height(), dabRect.width());

                              cgh.parallel_for(imageRange, [=](id<2> idx) {
                                  quint8 *dabPixel = dabImageDevice + pixelSize * (idx[0] * dabRect.width() + idx[1]);
                                  processPixel<false>(dabPixel, idx[1], idx[0], &gen, &data, channelFlags, opacity);
                              });
                          });

                          dabEvent = prefillEvent;
                          numDabsGenerated++;
                      }

                      auto newEvent = q.submit([&](handler& cgh) {
                          // Calling use_kernel_bundle() causes the parallel_for() below to use the
                          // pre-compiled kernel from "myBundle".
                          cgh.use_kernel_bundle(myBundle);

                          if (dabEvent) {
                              cgh.depends_on(*dabEvent);
                          }

                          range<2> imageRange(dabRect.height(), dabRect.width());
                          id<2> dabOffset(y, x);

                          cgh.parallel_for(imageRange, [=](id<2> idx) {
                              id<2> imagePos = idx + dabOffset;

                              quint8 *dabPixel = dabImageDevice + pixelSize * (idx[0] * dabRect.width() + idx[1]);
                              quint8 *vectorPixel = fullImageDevice + pixelSize * (imagePos[0] * imageRect.width() + imagePos[1]);
                              composePixel(dabPixel, vectorPixel, &data, opacity, channelFlags);
                          });
                      });

                      dabEvent = newEvent;
                  }

                  numDabsPainted++;
              }
          }

          auto transferTask = q.submit([&](handler& cgh) {
              cgh.depends_on(*dabEvent);
              cgh.memcpy(fullImageHost, fullImageDevice, bufferSize);
          });

          q.wait();

          if (mode == TwoStageTwoPass) {
              std::cout << "Cache hit rate: " << qreal(cachedDabsFraction.first) / cachedDabsFraction.second << std::endl;
              std::cout << "Generated dabs: " << numDabsGenerated << std::endl;
          }
          std::cout << "Painted dabs: " << numDabsPainted << std::endl;

          {
              auto elapsed = std::chrono::high_resolution_clock::now() - start;

              long long milliseconds =
                  std::chrono::duration_cast<std::chrono::milliseconds>(elapsed)
                      .count();

              std::cout << "Total time: " << milliseconds << "ms" << std::endl;
          }

          auto clearTask = q.submit([&](handler& cgh) {
              cgh.depends_on(*dabEvent);
              cgh.memset(fullImageDevice, 0, bufferSize);
          });

          q.wait();

#ifdef HAVE_SPNG

          std::string fileModeString = modeString;
          std::replace(fileModeString.begin(), fileModeString.end(), ' ', '_');
          fileModeString.erase(std::remove(fileModeString.begin(), fileModeString.end(), ','), fileModeString.end());

          if (mode == TwoStageTwoPass) {
              fileModeString += "_" + std::to_string(qreal(cachedDabsFraction.first) / cachedDabsFraction.second);
          }

          const std::string filename = "result_image_" + fileModeString + ".png";

          std::cout << "Saving into: " << filename << std::endl;

          PNGEncoder enc(filename);

          const int result = enc.encode(fullImageHost, imageRect.width(), imageRect.height());

          //std::cout << "done saving: " << result << std::endl;

#endif // HAVE_SPNG

    }

    std::cout << "==========================" << std::endl;

    free(fullImageDevice, q);
    free(dabImageDevice, q);
    free(fullImageHost, q);
  } catch (exception const &e) {
    std::cout << "An exception is caught while adding two vectors.\n";
    std::cout << e.what() << std::endl;
    std::terminate();
  }

  std::cout << "Vector add successfully completed on device\n";
  return 0;
}
