#ifndef KIS_GLOBAL_H
#define KIS_GLOBAL_H

#define KIS_M_PI       3.14159265358979323846f   // pi

template<typename T>
inline T pow2(const T& x) {
    return x * x;
}


#endif // KIS_GLOBAL_H
