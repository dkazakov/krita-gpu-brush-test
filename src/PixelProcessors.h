#ifndef PIXELPROCESSORS_H
#define PIXELPROCESSORS_H

#include "KoOptimizedCompositeOpOver32.h"


struct MaskProcessingData {
    MaskProcessingData(const quint8 *_color,
                       const int _pixelSize,
                       float _randomness,
                       float _density,
                       float _centerX,
                       float _centerY,
                       float _angle)
        : color(_color)
        , pixelSize(_pixelSize)
        , randomness(_randomness)
        , density(_density)
        , centerX(_centerX)
        , centerY(_centerY)
        , cosa(std::cos(_angle))
        , sina(std::sin(_angle))
    {
    }

    const quint8* color;
    const int pixelSize;
    float randomness;
    float density;
    float centerX;
    float centerY;

    float cosa;
    float sina;
};


template <bool doCompose>
ALWAYS_INLINE
    void processPixel(quint8* dabPointer,
                 int x, int y,
                 const TrivialCircleMaskGenerator *maskGenerator,
                 const MaskProcessingData *m_d,
                 const std::bitset<4> &channelFlags,
                 float opacity)
{
    float random = 1.0f;
    quint8 alphaValue = 0;
    // this offset is needed when brush size is smaller then fixed device size
    int supersample = 1;
    float invss = 1.0f / supersample;
    int samplearea = pow2(supersample);

    int value = 0;
    //  for (int sy = 0; sy < supersample; sy++) {
    //      for (int sx = 0; sx < supersample; sx++) {
    //          float x_ = x + sx * invss - m_d->centerX;
    //          float y_ = y + sy * invss - m_d->centerY;
    //          float maskX = m_d->cosa * x_ - m_d->sina * y_;
    //          float maskY = m_d->sina * x_ + m_d->cosa * y_;
    //          value += maskGenerator->valueAt(maskX, maskY);
    //      }
    //  }
    {
        float x_ = x  - m_d->centerX;
        float y_ = y  - m_d->centerY;
        float maskX = m_d->cosa * x_ - m_d->sina * y_;
        float maskY = m_d->sina * x_ + m_d->cosa * y_;
        value = maskGenerator->valueAt(maskX, maskY);
    }

    //  if (supersample != 1)
    //      value /= samplearea;

    //  if (m_d->randomness != 0.0f) {
    //      //random = (1.0 - m_d->randomness) + m_d->randomness * randomSource->generateNormalized();
    //  }

    alphaValue = quint8((255 - value) * random);

    //  // avoid computation of random numbers if density is full
    //  if (m_d->density != 1.0f) {
    //      // compute density only for visible pixels of the mask
    //      if (alphaValue != 0) {
    //          if (!(m_d->density >= randomSource->generateNormalized())) {
    //              alphaValue = 0;
    //          }
    //      }
    //  }


    if constexpr (doCompose) {
        OverCompositor32<quint8, quint32, false, true>::
            compositeOnePixelScalar<true, int>(m_d->color, dabPointer, &alphaValue, opacity, channelFlags);

    } else {
        memcpy(dabPointer, m_d->color, static_cast<size_t>(m_d->pixelSize));
        dabPointer[3] = rint(float(dabPointer[3]) * alphaValue / 255.0f);
    }
}

ALWAYS_INLINE
    void composePixel(quint8* dabPointer,
                 quint8* dstPixel,
                 const MaskProcessingData *m_d,
                 float opacity,
                 const std::bitset<4> &channelFlags)
{
    OverCompositor32<quint8, quint32, false, true>::
        compositeOnePixelScalar<false, int>(dabPointer, dstPixel, nullptr, opacity, channelFlags);
}

#endif // PIXELPROCESSORS_H
