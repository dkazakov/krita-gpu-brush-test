#include <sycl/sycl.hpp>
#include <array>
#include <iostream>
#include <string>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/sum.hpp>

#include <QSize>
#include <QRect>

#include "TrivialCircleMaskGenerator.h"
#include "PixelProcessors.h"

#include <config-spng.h>

#ifdef HAVE_SPNG
#include "PNGEncoder.h"
#endif // HAVE_SPNG

#define USE_FALLBACK_DATA_TRANSFER_ROUTINE

using namespace sycl;

// Create an exception handler for asynchronous SYCL exceptions
static auto exception_handler = [](sycl::exception_list e_list) {
  for (std::exception_ptr const &e : e_list) {
    try {
      std::rethrow_exception(e);
    }
    catch (std::exception const &e) {
      std::cout << "Failure" << std::endl;
      std::terminate();
    }
  }
};

#ifdef USE_FALLBACK_DATA_TRANSFER_ROUTINE

/**
 * Implementation of Intel's memcpy2d has two bugs, so we replace it
 * with a fallback function. The bugs are:
 *
 * 1) It doesn't support profiling
 * 2) It is much slower than performing copying manually using
 *    this function.
 */

namespace sycl {

template <typename T>
void commonUSMCopy2DFallbackKernelXXX(handler &cgh, const void *Src, size_t SrcPitch,
                                      void *Dest, size_t DestPitch, size_t Width,
                                      size_t Height) {
    // Otherwise the data is accessible on the device so we do the operation
    // there instead.
    // Limit number of work items to be resistant to big copies.
    id<2> Chunk = cgh.computeFallbackKernelBounds(Height, Width);
    id<2> Iterations = (Chunk + id<2>{Height, Width} - 1) / Chunk;
    cgh.parallel_for<class __usmmemcpy2d<T>>(
        range<2>{Chunk[0], Chunk[1]}, [=](id<2> Index) [[intel::kernel_args_restrict]] {
            T *CastedDest = static_cast<T *>(Dest);
            const T *CastedSrc = static_cast<const T *>(Src);
            for (uint32_t I = 0; I < Iterations[0]; ++I) {
                for (uint32_t J = 0; J < Iterations[1]; ++J) {
                    id<2> adjustedIndex = Index + Chunk * id<2>{I, J};
                    if (adjustedIndex[0] < Height && adjustedIndex[1] < Width) {
                        CastedDest[adjustedIndex[0] * DestPitch + adjustedIndex[1]] =
                            CastedSrc[adjustedIndex[0] * SrcPitch + adjustedIndex[1]];
                    }
                }
            }
        });
}

}

#endif /* USE_FALLBACK_DATA_TRANSFER_ROUTINE */

enum AllocationType {
    DeviceAllocation,
    HostAllocation,
    SharedAllocation
};


int doBrushPainting(AllocationType allocType)
{
    const std::string modeString =
        allocType == DeviceAllocation ? "device" :
            allocType == HostAllocation ? "host" :
            "shared";

    std::cout << "#######################################" << std::endl;
    std::cout << "Begin rendering in mode: " << modeString << std::endl;
    std::cout << "#######################################" << std::endl;

    auto selector = default_selector_v;

    const int pixelSize = 4;

    QRect imageRect(0, 0, 5000, 5000);
    QRect dabRect(0, 0, 500, 500);
    const size_t dabSize = pixelSize * dabRect.width() * dabRect.height();
    const size_t bufferSize = pixelSize * imageRect.width() * imageRect.height();
    std::vector<quint8> color({255, 0, 0, 255});

    TrivialCircleMaskGenerator gen(dabRect.width(), 0.2, 0.9, 0.7, 2, true);

    try {
        queue q(selector, exception_handler, {property::queue::enable_profiling()});

        // Print out the device information used for the kernel code.
        std::cout << "Running on device: "
                  << q.get_device().get_info<info::device::name>() << "\n";

        std::cout << "Start allocation" << std::endl;

        quint8 *fullImageDevice = nullptr;

        switch (allocType) {
        case DeviceAllocation:
            fullImageDevice = malloc_device<quint8>(bufferSize, q);
            break;
        case HostAllocation:
            fullImageDevice = malloc_host<quint8>(bufferSize, q);
            break;
        case SharedAllocation:
            fullImageDevice = malloc_shared<quint8>(bufferSize, q);
            break;
        }

        quint8 *dabImageDevice = malloc_device<quint8>(dabSize, q);
        quint8 *fullImageHost = malloc_host<quint8>(bufferSize, q);

        if (!fullImageDevice || !fullImageHost || !dabImageDevice) {
            if (fullImageDevice) free(fullImageDevice, q);
            if (fullImageHost) free(fullImageDevice, q);
            if (dabImageDevice) free(dabImageDevice, q);

            std::cout << "Memory allocation failure.\n" << std::endl;
            return -1;
        }
        std::cout << "Start cloning color" << std::endl;

        quint8 *deviceColor = malloc_device<quint8>(pixelSize, q);
        assert(deviceColor);
        q.memcpy(deviceColor, color.data(), pixelSize);
        q.wait();

        std::cout << "End cloning color" << std::endl;

        // precompile all the kernels of the context
        auto myBundle = get_kernel_bundle<bundle_state::executable>(q.get_context());

        const std::vector<int> dabsPerUpdatePlan({1, 5, 10, 15, 20, 40, 80, 160});

        for (int dabsPerUpdate : dabsPerUpdatePlan) {

            std::cout << "==========================" << std::endl;
            std::cout << "Start compute, dabs per update: " << dabsPerUpdate << std::endl;

            auto start = std::chrono::high_resolution_clock::now();


            std::vector<event> dabEvents;
            std::vector<event> transferEvents;

            QRect currentUpdateRect;
            std::optional<event> dabEvent;

            int numDabsPainted = 0;
            int numUpdates = 0;

            for (int y = 0; y < 4500; y += 10) {
                for (int x = 0; x < 4500; x += 10) {
                    float angle = (x + y) * (KIS_M_PI / 6);

                    const float opacity = 0.0f + float((x)) / 4500;

                    MaskProcessingData data(deviceColor, pixelSize,
                                            0.0, 1.0,
                                            0.5 * dabRect.width(), 0.5 * dabRect.width(), angle);
                    std::bitset<4> channelFlags(ULLONG_MAX);


                    auto newEvent = q.submit([&](handler& cgh) {
                        // Calling use_kernel_bundle() causes the parallel_for() below to use the
                        // pre-compiled kernel from "myBundle".
                        cgh.use_kernel_bundle(myBundle);

                        if (dabEvent) {
                            cgh.depends_on(*dabEvent);
                        }

                        const QRect currentDab = dabRect.translated(QPoint(x, y));

                        if (currentUpdateRect.isEmpty()) {
                            currentUpdateRect = currentDab;
                        } else {
                            currentUpdateRect.setLeft(std::min(currentUpdateRect.left(), currentDab.left()));
                            currentUpdateRect.setRight(std::max(currentUpdateRect.right(), currentDab.right()));
                            currentUpdateRect.setTop(std::min(currentUpdateRect.top(), currentDab.top()));
                            currentUpdateRect.setBottom(std::max(currentUpdateRect.bottom(), currentDab.bottom()));
                        }

                        range<2> imageRange(dabRect.height(), dabRect.width());
                        id<2> dabOffset(y, x);

                        cgh.parallel_for(imageRange, [=](id<2> idx) {
                            id<2> imagePos = idx + dabOffset;

                            quint8 *vectorPixel = fullImageDevice + pixelSize * (imagePos[0] * imageRect.width() + imagePos[1]);
                            processPixel<true>(vectorPixel, idx[1], idx[0], &gen, &data, channelFlags, opacity);
                        });
                    });

                    dabEvents.push_back(newEvent);
                    dabEvent = newEvent;

                    if ((numDabsPainted % dabsPerUpdate) == 0 || x >= 4490) {
                        //                      std::cout << "doing update " << numDabsPainted << std::endl;
                        //                      std::cout << "x " << currentUpdateRect.x() << " "
                        //                                << "y " << currentUpdateRect.y() << " "
                        //                                << "width " << currentUpdateRect.width() << " "
                        //                                << "height " << currentUpdateRect.height() << " "
                        //                                << std::endl;

                        if (currentUpdateRect.width() > 500 + (dabsPerUpdate - 1) * 10) {
                            std::cout << "update rect sanity check failed. rect width is " << currentUpdateRect.width() << std::endl;
                            exit(1);
                        }


#ifdef USE_FALLBACK_DATA_TRANSFER_ROUTINE

                        const int rowStride = imageRect.width() * pixelSize;

                        auto transferEvent = q.submit([&](handler& cgh) {
                            cgh.depends_on(*dabEvent);

                            const quint8 *srcPtr = fullImageDevice +
                                                   pixelSize * (
                                                       currentUpdateRect.y() * imageRect.width() +
                                                       currentUpdateRect.x());

                            quint8 *dstPtr = fullImageHost +
                                             pixelSize * (
                                                 currentUpdateRect.y() * imageRect.width() +
                                                 currentUpdateRect.x());

                            commonUSMCopy2DFallbackKernelXXX<unsigned char>(cgh,
                                                                            srcPtr, rowStride,
                                                                            dstPtr, rowStride,
                                                                            currentUpdateRect.width() * pixelSize,
                                                                            currentUpdateRect.height());
                        });
#else
                        const int rowStride = imageRect.width() * pixelSize;

                        auto transferEvent = q.submit([&](handler& cgh) {
                            cgh.depends_on(*dabEvent);

                            const quint8 *srcPtr = fullImageDevice +
                                                   pixelSize * (
                                                       currentUpdateRect.y() * imageRect.width() +
                                                       currentUpdateRect.x());

                            quint8 *dstPtr = fullImageHost +
                                             pixelSize * (
                                                 currentUpdateRect.y() * imageRect.width() +
                                                 currentUpdateRect.x());

                            cgh.ext_oneapi_memcpy2d(dstPtr, rowStride,
                                                    srcPtr, rowStride,
                                                    currentUpdateRect.width() * pixelSize,
                                                    currentUpdateRect.height());
                        });
#endif


                        transferEvents.push_back(transferEvent);
                        dabEvent = transferEvent;
                        currentUpdateRect = QRect();
                        numUpdates++;
                    }

                    numDabsPainted++;
                }
            }



            q.wait();

            std::cout << "Painted dabs: " << numDabsPainted << std::endl;
            std::cout << "Updates transferred: " << numUpdates << std::endl;

            {
                auto elapsed = std::chrono::high_resolution_clock::now() - start;

                long long milliseconds =
                    std::chrono::duration_cast<std::chrono::milliseconds>(elapsed)
                        .count();

                std::cout << "Total time: " << milliseconds << "ms" << std::endl;
                std::cout << "Theoretical FPS: " << qreal(numDabsPainted) * 1000.0 / milliseconds << std::endl;
                std::cout << "Real FPS: " << qreal(numUpdates) * 1000.0 / milliseconds << std::endl;
            }

            {
                typedef boost::accumulators::stats<
                    boost::accumulators::tag::sum,
                    boost::accumulators::tag::min,
                    boost::accumulators::tag::max,
                    boost::accumulators::tag::mean,
                    boost::accumulators::tag::variance> stats;

                {
                    boost::accumulators::accumulator_set<uint64_t, stats> acc;

                    for (const auto &event : std::as_const(dabEvents)) {
                        const uint64_t start = event.get_profiling_info<info::event_profiling::command_start>();
                        const uint64_t end = event.get_profiling_info<info::event_profiling::command_end>();

                        acc((end - start) / 1000);
                    }

                    std::cout << "Dab time (avg): " << boost::accumulators::mean(acc) << " us" << std::endl;
                    std::cout << "Dab time (var): " << std::sqrt(boost::accumulators::variance(acc)) << " us" << std::endl;
                    std::cout << "Dab time (min): " << boost::accumulators::min(acc) << " us" << std::endl;
                    std::cout << "Dab time (max): " << boost::accumulators::max(acc) << " us" << std::endl;
                    std::cout << "Dab time (sum) " << std::rint(boost::accumulators::sum(acc) / 1000.0) << " ms" << std::endl;
                }

                {
                    boost::accumulators::accumulator_set<uint64_t, stats> acc;

                    for (const auto &event : std::as_const(transferEvents)) {
                        const uint64_t start = event.get_profiling_info<info::event_profiling::command_start>();
                        const uint64_t end = event.get_profiling_info<info::event_profiling::command_end>();

                        acc((end - start) / 1000);
                    }

                    std::cout << "Transf time (avg): " << boost::accumulators::mean(acc) << " us" << std::endl;
                    std::cout << "Transf time (var): " << std::sqrt(boost::accumulators::variance(acc)) << " us" << std::endl;
                    std::cout << "Transf time (min): " << boost::accumulators::min(acc) << " us" << std::endl;
                    std::cout << "Transf time (max): " << boost::accumulators::max(acc) << " us" << std::endl;
                    std::cout << "Transf time (sum) " << std::rint(boost::accumulators::sum(acc) / 1000.0) << " ms" << std::endl;
                }


            }

            q.wait();

            auto clearTask = q.submit([&](handler& cgh) {
                cgh.depends_on(*dabEvent);
                cgh.memset(fullImageDevice, 0, bufferSize);
            });

            q.wait();

#ifdef HAVE_SPNG
            const std::string filename = "data-transfers-" + modeString + "-dpu-" + std::to_string(dabsPerUpdate) + ".png";

            std::cout << "Saving into: " << filename << std::endl;

            PNGEncoder enc(filename);

            const int result = enc.encode(fullImageHost, imageRect.width(), imageRect.height());

//std::cout << "done saving: " << result << std::endl;

#endif // HAVE_SPNG

        }

        std::cout << "==========================" << std::endl;

        free(fullImageDevice, q);
        free(dabImageDevice, q);
        free(fullImageHost, q);
        free(deviceColor, q);
    } catch (exception const &e) {
        std::cout << "An exception is caught while adding two vectors.\n";
        std::cout << e.what() << std::endl;
        std::terminate();
    }

    return 0;
}

int main(int argc, char* argv[]) {

  int retval = -1;

  std::vector<AllocationType> allocTypes =
      {DeviceAllocation, HostAllocation, SharedAllocation};

  for (AllocationType allocType : allocTypes) {
      retval = doBrushPainting(allocType);
      if (retval) {
        std::cout << "Failed to run benchmark in mode \"" << allocType << "\"\n";
      }
  }

  std::cout << "Vector add successfully completed on device\n";
  return 0;
}
