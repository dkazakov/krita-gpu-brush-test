/*
 *  SPDX-FileCopyrightText: 2008-2009 Cyrille Berger <cberger@cberger.net>
 *  SPDX-FileCopyrightText: 2010 Lukáš Tvrdý <lukast.dev@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef _KIS_MASK_GENERATOR_H_
#define _KIS_MASK_GENERATOR_H_

#include <QScopedPointer>

#include <string>

static const int OVERSAMPLING = 4;

/**
 * This is the base class for mask shapes
 * You should subclass it if you want to create a new
 * shape.
 */
class KisMaskGenerator
{
public:
    enum Type {
        CIRCLE, RECTANGLE
    };
public:

    /**
     * This function creates an auto brush shape with the following values:
     * @param radius radius
     * @param ratio aspect ratio
     * @param fh horizontal fade
     * @param fv vertical fade
     * @param spikes number of spikes
     * @param antialiasEdges whether to antialias edges
     * @param type type
     * @param id the brush identifier
     */
    KisMaskGenerator(qreal radius, qreal ratio, qreal fh, qreal fv, int spikes, bool antialiasEdges, Type type, const std::string& id);
    KisMaskGenerator(const KisMaskGenerator &rhs);

    virtual ~KisMaskGenerator();

    virtual KisMaskGenerator* clone() const = 0;

private:

    void init();

public:
    /**
     * @return the alpha value at the position (x,y)
     */
    virtual quint8 valueAt(qreal x, qreal y) const = 0;

    virtual bool shouldSupersample() const;

    virtual bool shouldSupersample6x6() const;

    virtual bool shouldVectorize() const;

    qreal width() const;

    qreal height() const;

    qreal diameter() const;    
    void setDiameter(qreal value);

    qreal ratio() const;
    qreal horizontalFade() const;
    qreal verticalFade() const;
    int spikes() const;
    Type type() const;
    bool isEmpty() const;
    void fixRotation(qreal &xr, qreal &yr) const;
    
    inline std::string id() const { return m_id; }
    inline std::string name() const { return m_id; }

    qreal softness() const;
    virtual void setSoftness(qreal softness);
    
    std::string curveString() const;
    void setCurveString(const std::string &curveString);

    bool antialiasEdges() const;
    virtual void setScale(qreal scaleX, qreal scaleY);

protected:
    qreal effectiveSrcWidth() const;
    qreal effectiveSrcHeight() const;

private:
    struct Private;
    const QScopedPointer<Private> d;
    std::string m_id;
};

#endif
