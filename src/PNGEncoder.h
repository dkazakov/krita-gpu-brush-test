#ifndef PNGENCODER_H
#define PNGENCODER_H

#include <string>

class PNGEncoder
{
public:
    PNGEncoder(std::string filename);
    int encode(void *buf, int width, int height);

private:
    std::string m_filename;
};

#endif // PNGENCODER_H
