/*
 * SPDX-FileCopyrightText: 2006 Cyrille Berger <cberger@cberger.net>
 * SPDX-FileCopyrightText: 2011 Silvio Heinrich <plassy@web.de>
 * SPDX-FileCopyrightText: 2022 L. E. Segovia <amy@amyspark.me>
 *
 * SPDX-License-Identifier: LGPL-2.0-or-later
 */

#ifndef KOOPTIMIZEDCOMPOSITEOPOVER32_H_
#define KOOPTIMIZEDCOMPOSITEOPOVER32_H_

#include <math.h>

#include <QtGlobal>
#include <bitset>
#include "KoAlwaysInline.h"
#include "KoStreamedMath.h"


template<typename channels_type, typename pixel_type, bool alphaLocked, bool allChannelsFlag>
struct OverCompositor32 {
    struct ParamsWrapper {
        ParamsWrapper(const std::bitset<4>& params)
            : channelFlags(params)
        {
        }
        const std::bitset<4> &channelFlags;
    };

    template <bool haveMask, typename _impl>
    static ALWAYS_INLINE void compositeOnePixelScalar(const channels_type *src, channels_type *dst, const quint8 *mask, float opacity, const ParamsWrapper &oparams)
    {
        const qint32 alpha_pos = 3;

        const float uint8Rec1 = 1.0f / 255.0f;
        const float uint8Max = 255.0f;

        float srcAlpha = src[alpha_pos];
        srcAlpha *= opacity;

        if (haveMask) {
            srcAlpha *= float(*mask) * uint8Rec1;
        }

        if (srcAlpha != 0.0f) {

            float dstAlpha = dst[alpha_pos];
            float srcBlendNorm = NAN;

            if (alphaLocked || dstAlpha == uint8Max) {
                srcBlendNorm = srcAlpha * uint8Rec1;
            } else if (dstAlpha == 0.0f) {
                dstAlpha = srcAlpha;
                srcBlendNorm = 1.0f;

                if (!allChannelsFlag) {
                    auto *d = reinterpret_cast<pixel_type*>(dst);
                    *d = 0; // dstAlpha is already null
                }
            } else {
                dstAlpha += (uint8Max - dstAlpha) * srcAlpha * uint8Rec1;
                // Optimized version of:
                //     srcBlendNorm = srcAlpha / dstAlpha);
                srcBlendNorm = OptiDiv<_impl>::divScalar(srcAlpha, dstAlpha);
            }

            if(allChannelsFlag) {
                if (srcBlendNorm == 1.0f) {
                    if (!alphaLocked) {
                        const auto *s = reinterpret_cast<const pixel_type*>(src);
                        auto *d = reinterpret_cast<pixel_type*>(dst);
                        *d = *s;
                    } else {
                        dst[0] = src[0];
                        dst[1] = src[1];
                        dst[2] = src[2];
                    }
                } else if (srcBlendNorm != 0.0f){
                    dst[0] = KoStreamedMath<_impl>::lerp_mixed_u8_float(dst[0], src[0], srcBlendNorm);
                    dst[1] = KoStreamedMath<_impl>::lerp_mixed_u8_float(dst[1], src[1], srcBlendNorm);
                    dst[2] = KoStreamedMath<_impl>::lerp_mixed_u8_float(dst[2], src[2], srcBlendNorm);
                }
            } else {
                const std::bitset<4> &channelFlags = oparams.channelFlags;

                if (srcBlendNorm == 1.0f) {
                    if(channelFlags[0]) dst[0] = src[0];
                    if(channelFlags[1]) dst[1] = src[1];
                    if(channelFlags[2]) dst[2] = src[2];
                } else if (srcBlendNorm != 0.0f) {
                    if(channelFlags[0]) dst[0] = KoStreamedMath<_impl>::lerp_mixed_u8_float(dst[0], src[0], srcBlendNorm);
                    if(channelFlags[1]) dst[1] = KoStreamedMath<_impl>::lerp_mixed_u8_float(dst[1], src[1], srcBlendNorm);
                    if(channelFlags[2]) dst[2] = KoStreamedMath<_impl>::lerp_mixed_u8_float(dst[2], src[2], srcBlendNorm);
                }
            }

            if (!alphaLocked) {
                dst[alpha_pos] = KoStreamedMath<_impl>::round_float_to_u8(dstAlpha);
            }
        }
    }
};


#endif // KOOPTIMIZEDCOMPOSITEOPOVER32_H_
