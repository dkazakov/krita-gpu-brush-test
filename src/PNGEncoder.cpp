#include "PNGEncoder.h"

#include <inttypes.h>
#include <spng.h>

int encode_image(void *image, size_t length, uint32_t width, uint32_t height,
                 enum spng_color_type color_type, int bit_depth,
                 size_t *result_size,
                 void **result_buf)
{
    int fmt;
    int ret = 0;
    spng_ctx *ctx = NULL;
    struct spng_ihdr ihdr = {0}; /* zero-initialize to set valid defaults */

    /* Creating an encoder context requires a flag */
    ctx = spng_ctx_new(SPNG_CTX_ENCODER);

    /* Encode to internal buffer managed by the library */
    spng_set_option(ctx, SPNG_ENCODE_TO_BUFFER, 1);

    /* Alternatively you can set an output FILE* or stream with spng_set_png_file() or spng_set_png_stream() */

    /* Set image properties, this determines the destination image format */
    ihdr.width = width;
    ihdr.height = height;
    ihdr.color_type = color_type;
    ihdr.bit_depth = bit_depth;
    /* Valid color type, bit depth combinations: https://www.w3.org/TR/2003/REC-PNG-20031110/#table111 */

    spng_set_ihdr(ctx, &ihdr);

    /* When encoding fmt is the source format */
    /* SPNG_FMT_PNG is a special value that matches the format in ihdr */
    fmt = SPNG_FMT_PNG;

    /* SPNG_ENCODE_FINALIZE will finalize the PNG with the end-of-file marker */
    ret = spng_encode_image(ctx, image, length, fmt, SPNG_ENCODE_FINALIZE);

    if(ret)
    {
        printf("spng_encode_image() error: %s\n", spng_strerror(ret));
        goto encode_error;
    }

    {
        size_t png_size;
        void *png_buf = NULL;

        /* Get the internal buffer of the finished PNG */
        png_buf = spng_get_png_buffer(ctx, &png_size, &ret);

        if (ret != 0) {
            printf("spng_get_png_buffer() result is incorrect: %s\n", spng_strerror(ret));
        }

        if(png_buf == NULL)
        {
            printf("spng_get_png_buffer() error: %s\n", spng_strerror(ret));
        }

        /* User owns the buffer after a successful call */
        //free(png_buf);

        *result_size = png_size;
        *result_buf = png_buf;
    }

encode_error:

    spng_ctx_free(ctx);

    return ret;
}

PNGEncoder::PNGEncoder(std::string filename)
    : m_filename(std::move(filename))
{
}

#include <fstream>
#include <iostream>

int PNGEncoder::encode(void *buf, int width, int height)
{
    size_t png_size = 0;
    void *png_buf = NULL;

    int result = encode_image(buf, 4 * width * height, width, height,
                              SPNG_COLOR_TYPE_TRUECOLOR_ALPHA, 8,
                              &png_size, &png_buf);

    if (png_buf) {
        //std::cout << "sng_size " << png_size << std::endl;

        std::ofstream stream(m_filename,
                             std::ios_base::out |
                                 std::ios::trunc |
                                 std::ios_base::binary);



        stream.write((char*)png_buf, png_size);
        stream.flush();

        //std::cout << "write completed" << std::endl;

        free(png_buf);
    }

    return result;
}
